<?php

namespace App\Http\Controllers;

use App\Post;
use Dotenv\Validator;
use Faker\Provider\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PostsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('postoperator')) {
            $post = Post::all();
            return view('posts.index',compact('post'));
        }

        return new JsonResponse('forbidden', Response::HTTP_FORBIDDEN);
    }
    public function create()
    {
        return view('posts.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'image' => 'mimes:jpeg,png,jpg|max:2048'
        ]);

       if ($request->hasFile('image')) {
           $input['image'] = time() . '.' . $request->file('image')->getClientOriginalExtension();
           $destinationPath = public_path('/images');
           $test = $request->file('image')->move($destinationPath, $input['image']);
       }else{$input['image'] = NULL;}

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->image = $input['image'];
        $post->user_id = Auth::id();
        $post->save();

        return redirect()->route('show-post',['id'=>$post->id]);
    }
    public function show($id)
    {
       $post = Post::find($id);
       return view('posts.show',compact('post'));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit',compact('post'));
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $this->validate(request(),[
            'title' => 'required',
            'body' => 'required',
        ]);
        $post->update($request->all());
        return redirect()->route('show-post',['id'=>$post->id]);
    }

    public function destroy($id)
    {
        Post::find($id)->delete();
        return back();
    }
}
