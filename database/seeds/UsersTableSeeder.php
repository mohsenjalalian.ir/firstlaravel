<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminPass = 'qwert+98765';
        \Illuminate\Support\Facades\DB::insert('insert into users (`name`, `email`, `password`) values (?, ?, ?)', ['admin', 'admin@admin.ir', bcrypt($adminPass)]);

    }
}
