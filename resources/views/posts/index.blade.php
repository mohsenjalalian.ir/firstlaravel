@extends('layout')
<div><a class="btn btn-small btn-success" href="/">Home</a></div>
<div><a class="btn btn-small btn-success" href="posts.create">Create New Post</a></div>
<div style="height:30px;"></div>
<table class="table table-striped table-bordered" style="border:2px solid black;padding:20px;">
    <thead >
    <tr>
        <td style="border:2px solid black;padding:10px;">ID</td>
        <td style="border:2px solid black;padding:10px;">Title</td>
        <td style="border:2px solid black;padding:10px;">Body</td>
        <td style="border:2px solid black;padding:10px;">User</td>

    </tr>
    </thead>
    <tbody>
    @foreach($post as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->body }}</td>
            <td>{{ $value->user->name }}</td>

            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('posts/' . $value->id) }}">Show this Post</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                {{--<a class="btn btn-small btn-info" href="{{ URL::to('posts/' . $value->id . '/edit') }}">Edit this Post</a>--}}

                {{--<a class="btn btn-small btn-info" href="{{ URL::to('posts/' . $value->id . '/destroy') }}">Delete this Post</a>--}}

            </td>


            <td><a href="{{ route('edit-post', $value->id) }}" class="btn btn-warning">Edit</a></td>
            <td>
                <form action="{{ route('delete-post', $value->id) }}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
