@extends('layout')

@section('content')
    <h3><a href="{{ route('index-post') }}">Show All POST</a></h3>
    <h3>Edit {{ $post->id }}</h3>
    <form  method="POST" action="{{ route('update-post', $post->id) }}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">


        <div class="form-group">
            <label for="title" class="control-label">موضوع</label>

            <input id="title" type="text" class="form-control" name="title" value="{{ $post->title }}" required>
            @if($errors->has('title'))
                <span class="help-block">
              <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="body" class="control-label">بدنه</label>

            <textarea id="body" type="text" class="form-control" name="body" value="{{ $post->body }}" required>{{ $post->body }}</textarea>
            @if($errors->has('body'))
                <span class="help-block">
             <strong>{{ $errors->first('body') }}</strong>
           </span>
            @endif
        </div>


        <div class="form-group">

            <button type="submit" class="btn btn-primary">
                ثبت
            </button>

        </div>

    </form>

@endsection

