@role('admin')
@extends('layout')

@section('content')
    <h3><a href="{{ route('index-post') }}">Show All POST</a></h3>
    <h3>Create Post</h3>
<form  method="POST" action="{{ route('store-post') }}" enctype="multipart/form-data">
     {{ csrf_field() }}
      <div class="form-group">
      <label for="title" class="control-label">موضوع</label>

        <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required>
          @if($errors->has('title'))
            <span class="help-block">
              <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif

          </div>

          <div class="form-group">
          <label for="body" class="control-label">بدنه</label>

              <textarea id="body" type="text" class="form-control" name="body" value="{{ old('body') }}" required></textarea>
           @if($errors->has('body'))
           <span class="help-block">
             <strong>{{ $errors->first('body') }}</strong>
           </span>
            @endif
            </div>


    <div class="form-group">
        <label for="image" class="control-label">عکس</label>

        <input id="image" type="file" class="form-control" name="image" value="{{ old('image') }}" >
        @if($errors->has('images'))
            <span class="help-block">
              <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif

    </div>


            <div class="form-group">

            <button type="submit" class="btn btn-primary">
                                        ثبت
            </button>

            </div>

            </form>

@endsection

@endrole